package com.atlassian.tutorial.confluence.plugin.unittesting;

public interface MyPluginComponent
{
    String getName();
}
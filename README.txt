This tutorial shows you how to create unit tests for a Confluence plugin. 

You can find the full instructions for this tutorial here:

https://developer.atlassian.com/display/CONFDEV/Unit+Testing+Plugins
